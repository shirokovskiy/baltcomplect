<?php
session_start();

Define("IN_SITE",1);
require "conf.php";
$db_name = DB_NAME;
require "../sql.php";
require "lib/config.php";
require "lib/catalog.php";
require "lib/common.php";
require "lib/data.php";
require "lib/images_lib.php";
require "lib/messages.php";
require "lib/news.php";
require "lib/struct.php";
require "lib/files.php";
require "lib/gallery.php";
require "lib/photos.php";
require "lib/votings.php";
require "lib/articles.php";
require "lib/zakaz.php";
require "lib/questions.php";
//require "lib/image_func.php";
require "../func_lib/common.php";
require "../func_lib/data_f.php";
require "../func_lib/news_f.php";
require "../func_lib/votings_f.php";

if(isset($_GET["logout"]))
	$_SESSION["userd"]=null;

if(isset($_SESSION["userd"])){
	$us=split("\|",$_SESSION["userd"]);
	$user=get_from_base("*","users","login='".$us[0]."'","id");
	if($user[0]["passwd"]==$us[1])
		$user_data=$user[0];
	else
		header("Location:login.php?err=3");
}elseif(isset($_POST["login"])&&isset($_POST["passwd"])){
	$user=get_from_base("*","users","login='".$_POST["login"]."'","id");
	if(count($user)==1){
		if($user[0]["passwd"]==md5($_POST["passwd"])){
			$_SESSION["userd"]=$user[0]["login"]."|".$user[0]["passwd"];
			$user_data=$user[0];
		}else{
			header("Location:login.php?err=2");
		}
	}else{
		header("Location:login.php?err=1");
	}
}else{
	header("Location:login.php");
}




include("spaw2/spaw.inc.php");

switch (@$_GET["act"]){
	case "add_razdel": add_razdel(); break;
	case "delete_razdel": delete_razdel($_GET["id"]); break;
	case "rename_razdel": rename_razdel(); break;
	case "set_main": set_main($_GET["id"]); break;
	case "unset_main": mysql_query("update razdel set main=0 where id=".$_GET["id"]); break;
	case "change_content_type": change_content_type($_GET["content_type"],$_GET["from_razdel"]); break;
	case "change_navi_type": change_navi_type($_GET["navi_type"],$_GET["from_razdel"]); break;
	case "add_data": add_data($_POST["cont"],$_GET["from_razdel"]); break;

	case "add_catalog": add_catalog(); break;
	case "get_change_catalog": $change_catalog = get_change_catalog($_GET["id"]); break;
	case "change_catalog": change_catalog(); break;
	case "delete_catalog": delete_catalog($_GET["id"]); break;
	case "set_catalog_position": set_catalog_position($_POST["ch"]); break;

	case "change_photos": change_photos(); break;
	case "delete_photos": delete_photos($_GET["id"]); break;
	case "set_photos_position": set_photos_position($_POST["ch"]); break;
	case "get_change_photos": $change_photos = get_change_photos($_GET["id"]); break;
	case "add_photos": add_photos(); break;

	case "add_image": add_image(); break;
	case "delete_image": delete_image($_GET["name"]); break;
	case "change_pos": change_position($_GET["from_razdel"],$_GET["order"]); break;
	case "change_parent": change_parent($_GET["from_razdel"],$_GET["parent"]); break;
	case "set_alias": set_alias($_GET["from_razdel"],$_GET["alias_id"]); break;
	case "del_message": del_message($_GET["id"]); break;
	case "add_into_gallery": add_into_gallery($_POST["name"],$_POST["catalog_id"]); break;
	case "set_keywords": set_keywords($_GET["keywords"],$_GET["from_razdel"]); break;
	case "set_title": set_title($_GET["title"],$_GET["from_razdel"]); break;
	case "set_description": set_description($_GET["desc"],$_GET["from_razdel"]); break;
	case "read_dump": read_dump(); break;
	case "set_icon": set_icon($_GET["from_razdel"]); break;
	case "del_icon": del_icon($_GET["from_razdel"]); break;
	case "set_visible": set_visible($_GET["from_razdel"]); break;
	case "del_zakaz": del_zakaz($_GET["id"]); break;
	case "add_data": add_data($_POST["content"],$_GET["from_razdel"],$_POST["sub_link"]); break;
	case "add_file": add_file($_GET["from_razdel"]); break;
	case "attache_file_d": attache_file_d($_GET["from_razdel"]); break;
	case "delete_attache_file": delete_attache_file_d($_GET["id"]); break;
	case "add_gal_photo": add_gallery_photo($_GET["from_razdel"]); break;
	case "del_gal_photo": delete_gallery_photo($_GET["id"]); break;
	case "upload_catalog": upload_catalog(); break;

	case "add_vote_question": add_vote_question($_GET["name"]); break;
	case "rename_vote_question": rename_vote_question($_GET["name"],$_GET["question_id"]); break;
	case "delete_vote_question": delete_vote_question($_GET["question_id"]); break;
	case "add_vote_answer": add_vote_answer($_GET["name"],$_GET["question_id"]); break;
	case "rename_vote_answer": rename_vote_answer($_GET["name"],$_GET["answer_id"]); break;
	case "delete_vote_answer": delete_vote_answer($_GET["answer_id"]); break;

	case "publish": publish($_GET["id"]); break;
	case "hide": hide($_GET["id"]); break;
	case "get_single_question": $single_question = get_single_question($_GET["id"]); break;
	case "answer": answer($_POST["answer"],$_POST["id"]);break;
	case "delete_question": delete_question($_GET["id"]); break;
}

$path=array();
$page="empty.php";
$main_req = "view.php";

if(isset($_GET["work"]))
	switch($_GET["work"]){
		case "razdel":
			$r_struct = array();
			foreach($types as $k=>$v){
				$level=0;
				get_struct(0,$v[1],$v[0],$r_struct);
			}
			if(($show_razdel = get_by_id($_GET["from_razdel"],"razdel"))!==false){
				get_path($show_razdel["id"],$path);
				switch($show_razdel["content_type"]){
					case "data":
						$show_content = get_text_data($_GET["from_razdel"]);
						$content = new SpawEditor('cont',$show_content);
						$att_files = get_from_base("*","attache_files","razdel=".$show_razdel["id"],"id");
						break;
					case "catalog":
						$text = get_text_data($_GET["from_razdel"]);
						$content = new SpawEditor('cont',$text);
						$show_content = get_from_base("*","catalog","razdel=".$show_razdel["id"],"pos");
						if(isset($change_catalog))
							$opisanie = new SpawEditor('opisanie',$change_catalog["opisanie"]);
						else
							$opisanie = new SpawEditor('opisanie');
						break;
					case "form":
						$text = get_text_data($_GET["from_razdel"]);
						$content = new SpawEditor('cont',$text);
						$show_messages = get_messages();
						break;
					case "gallery":
						$data = get_from_base("*","gallery","razdel=".$_GET["from_razdel"],"id desc");
						break;
					case "photos":
						$text = get_text_data($_GET["from_razdel"]);
						$content = new SpawEditor('cont',$text);
						$show_content = get_from_base("*","photos","razdel=".$show_razdel["id"],"pos");
						if(isset($change_photos))
							$opisanie = new SpawEditor('opisanie',$change_photos["opisanie"]);
						else
							$opisanie = new SpawEditor('opisanie');
						break;
					case "articles":
						if(isset($_GET["act"]))
							switch($_GET["act"]){
								case "add": add_article($show_razdel["id"]); break;
								case "delete": if(isset($_GET["id"])) delete_article($_GET["id"]); break;
								case "change": change_article(); break;
								case "set_pars": set_articles_pars(); break;
							}
						if(isset($_GET["change"])&&is_numeric($_GET["change"]))
							$change_article=get_by_id($_GET["change"],"articles");
						$data = get_from_base("*","articles","razdel=".$show_razdel["id"],"pos");
						break;
					case "gb":
						if(isset($_GET["act"]))
							switch($_GET["act"]){
								case "delete": mysql_query("delete from gb where id='".$_GET["id"]."'"); break;
								case "publish": mysql_query("update gb set public=1 where id='".$_GET["id"]."'"); break;
								case "hide": mysql_query("update gb set public=0 where id='".$_GET["id"]."'"); break;
							}
						$data = get_from_base("*","gb","razdel=".$show_razdel["id"],"id desc");
						break;
					case "news":
						$content = new SpawEditor('cont');
						if(isset($_GET["act"]))
							switch($_GET["act"]){
								case "add": add_news($show_razdel["id"]); break;
								case "delete": delete_news($_GET["id"]); break;
								case "change": change_news($_POST["ch_id"]); break;
							}
						if(isset($_GET["change"])){
							$change_news=get_by_id($_GET["change"],"news");
							$content = new SpawEditor('cont',$change_news["content"]);
						}
						$data = get_from_base("*","news","razdel=".$show_razdel["id"],"id desc");
						break;
					case "questions":
						if(isset($single_question))
							$answer = new SpawEditor('answer',$single_question["answer"]);
						$show_content = get_questions();
						break;
				}
				$page="razdel.php";
			}
			break;
		case "news":
			if(isset($_GET["act"]))
				switch($_GET["act"]){
					case "add": add_news(); break;
					case "change": add_news("change"); break;
					case "delete":
						if(isset($_GET["id"]))
							delete_news($_GET["id"]);
						break;
				}

			if(isset($_GET["page"]))
				switch($_GET["page"]){
					case "add":
						$page = "news/add.php";
						break;
					case "change":
						if(isset($_GET["id"])&&is_numeric($_GET["id"])){
							$res = mysql_query("select * from news where id=".$_GET["id"]);
							if(mysql_num_rows($res)==1){
								$data = mysql_fetch_assoc($res);
								$page = "news/change.php";
							}
						}
						break;
				}
			else{
				$data = get_from_base("*","news","1","id desc");
				$page = "news/list.php";
			}
			break;
		case "images":
			$show_images = get_images();
			$main_req = "images.php";
			break;
		case "hidden_pages":
			$data = get_from_base("*","razdel","type='virtual'","id");
			$page = "hidden_pages/list.php";
			break;
		case "config":
			$_ctypes = array("struct_view"=>"text","contacts"=>"text","mail"=>"text","foto_preview_width"=>"text","foto_preview_height"=>"text","catalog_preview_width"=>"text","catalog_preview_height"=>"text");
			if(isset($_GET["act"])&&$_GET["act"]=="set")
				set_config($_ctypes);
			$data=array();
			$res = mysql_query("select * from config where 1");
			while($tmp = mysql_fetch_assoc($res))
				$data[$tmp["config_name"]]=$tmp["config_value"];
			$contacts= new SpawEditor('contacts',$data["contacts"]);
			$page = "config/main.php";
			break;
		case "votings":
			$questions = get_vote_questions();
			$page = "votings.php";
			break;
		case "single_voting":
			$question = get_vote_question_by_id($_GET["id"]);
			$vote_answers = get_voting_stat($_GET["id"]);
			$page = "single_voting.php";
			break;
		case "zakaz":
			$show_zakaz = get_zakaz();
			$page="zakaz.php";
			break;
		case "adm_users":
			$err=0;
			if(isset($_GET["act"]))
				switch($_GET["act"]){
					case "add":
						$err=0;
						if($_POST["passwd"]!=$_POST["passwd_re"])
							$err=1;
						if($err==0)
							mysql_query("insert into users(name,login,passwd,su) values('".$_POST["name"]."','".$_POST["login"]."','".md5($_POST["passwd"])."','".$_POST["su"]."')");
						echo mysql_error();
						break;
					case "edit":
						$ch=get_by_id($_GET["id"],"users");
						break;
					case "change":
						mysql_query("update users set name='".$_POST["name"]."',login='".$_POST["login"]."',su='".$_POST["su"]."' where id=".$_GET["id"]);
						if($_POST["passwd"]!=""&&$_POST["passwd"]==$_POST["passwd_re"]){
							mysql_query("update users set passwd='".md5($_POST["passwd"])."' where id=".$_GET["id"]);
							if($_GET["id"]==$user_data["id"])
								$_SESSION["user"]=$user_data["login"]."|".md5($_POST["passwd"]);
						}else{
							$err=3;
						}
						break;
					case "delete":
						mysql_query("delete from users where id=".$_GET["id"]);
						break;
				}
			if($user_data["su"]==2){
				$data=get_from_base("*","users","1","id");
			}else{
				$data=get_from_base("*","users","su<2","id");
			}
			$page="users/list.php";
			break;
		case "ch_passwd":
			if(isset($_GET["act"])&&$_GET["act"]=="set")
				if($_POST["passwd"]!=""&&$_POST["passwd"]===$_POST["passwd_re"]){
					mysql_query("update `users` set passwd='".md5($_POST["passwd"])."' where id=".$user_data["id"]);
					echo mysql_error();
					$_SESSION["user"]=null;
					session_destroy();
					header("Location: login.php");
				}else{
					$err=1;
				}
			$page="users/ch_pass.php";
			break;
	}
$s_v=0;
$res = mysql_query("select config_value from config where config_name='struct_view'");
if(mysql_num_rows($res)==1){
	$s_v = mysql_result($res,0,0);
}
if($s_v==0){
	$struct = array();
	foreach($types as $k=>$v){
		$level=0;
		//get_struct(0,$v[1],$v[0],$struct,$v[3]);
		$struct[$v[0]]=get_recursive_struct(0,$v[1],$v[3]);
	}
	//var_dump($struct);
}else{
	foreach($types as $k=>$v){
		$struct[$v[0]]["p0"]=array();
		$res = mysql_query("select * from razdel where type='".$v[1]."' order by position");
		while($tmp=mysql_fetch_assoc($res))
			$struct[$v[0]]["p".$tmp["parent"]][]=$tmp;
	}
}
$count_data = get_count();

header('Content-Type: text/html; charset=windows-1251');

require $main_req;
