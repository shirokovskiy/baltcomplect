<?php
function createNewEditor($name,$value="",$height="400",$width="100%"){
	$content = new FCKeditor($name) ;
	$content->BasePath = 'fckeditor/' ;
	$content->Config['CustomConfigurationsPath'] = '../myconfig.js' ;
	$content->Width  = $width ;
	$content->Height = $height ;
	$content->Value = $value ;
	$content->Config['SkinPath'] = 'skins/office2003/' ;
	return $content->CreateHtml();
}
