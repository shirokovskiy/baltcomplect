<?php
function add_image()
{
    if(file_exists($_FILES["image"]["tmp_name"])){
        copy($_FILES["image"]["tmp_name"],ROOT_DIR.SUB_DIR.IMAGES_LIB_DIR.basename($_FILES["image"]["name"]));
    }
}

function get_images()
{
    $arr = array();
    $dir = opendir(ROOT_DIR.SUB_DIR.IMAGES_LIB_DIR);
    while(($filename = readdir($dir))!==false){
        if($filename == ".."||$filename == ".") continue;
        $arr[] = $filename;
    }
    return $arr;
}

function delete_image($name)
{
    unlink(ROOT_DIR.SUB_DIR.IMAGES_LIB_DIR.$name);
}

