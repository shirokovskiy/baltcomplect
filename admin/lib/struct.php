<?php
/**
 * ����������, ���������� ������� ��� ���������� ���������� �����
 * @package admin_struct_lib
 * @author RedLineSoft
 * @copyright First Web Design Studio
 * @version 1.0
 */

/**
 * ���������� ������ ���������� ���������� �� �������� ����
 * ������� ������������� �� ���� ��������� ($type), ���� �� ������������� �������� ($parent)
 *
 * @param int $parent
 * @param string $type
 * @return array
 */
function get_menu($parent,$type=""){
    $arr = array();
    if(is_numeric($parent)){
        $where = "parent=".$parent;
        if($type!="")
            $where.=" and type='".$type."'";
        $arr = get_from_base("*","razdel",$where,"position");
    }
    return $arr;
}

/**
 * �������� �� ������� �������� ������� ����
 */
function have_children($parent){
    if(is_numeric($parent))
        if(mysql_num_rows(mysql_query("select id from razdel where parent=".$parent." limit 1"))==1)
            return true;
    return false;
}

/**
 * ��������� ������ �������� �� ���� ��������� ��� �� ������������� ��������
 */
function get_struct($par,$type,$type_name,&$target){
    $arr = get_menu($par,$type);
    for($i=0; $i<count($arr); $i++){
        $tmp=array("level" => $GLOBALS["level"]);
        $target[$type_name][] = array_merge($arr[$i],$tmp);
        if(have_children($arr[$i]["id"])){
            $GLOBALS["level"]++;
            get_struct($arr[$i]["id"],$type,$type_name,$target);
            $GLOBALS["level"]--;
        }
    }
}

/**
 * ��������� id ������������� ��������
 */
function get_parent($id){
    $par = -1;
    if(is_numeric($id)){
        $res = mysql_query("select parent from razdel where id=".$id);
        if(mysql_num_rows($res)==1)
            $par = mysql_result($res,0,0);
    }
    return $par;
}

//* ���������� ������, � ������� ������ ���� �� �����
function get_path($id,&$arr){
    $parent = get_parent($id);
    if($parent>0)
        get_path($parent,$arr);
    $arr[]=array(get_name_by_id($id),$id);
}

/**
 * ���������� ��� ������� �� id �������
 */
function get_name_by_id($id){
    $result = mysql_query("select name from razdel where id='".$id."'");
    if(mysql_num_rows($result))
        return mysql_result($result,0,0);
    return "";
}

function is_child($child,$parent,&$result){
    $cp = get_parent($child);
    if($cp!=-1){
        if($cp==$parent)
            $result = true;
        elseif($cp!=0&&$result==false)
            is_child($cp,$parent,$result);
    }
}

/**
 * ������� ���������� ������ �������. ��� ������ ����� �� GET-�������
 *
 */
function add_razdel(){
    $data = $_GET;
    if(isset($data["name"])&&trim($data["name"])!=""&&
        isset($data["parent"])&&is_numeric($data["parent"])&&
        isset($data["type"])&&trim($data["type"])!=""&&
        isset($data["content_type"])&&trim($data["content_type"])!="")
    {
        $max_pos = 1;
        $res = mysql_query("select max(position) from razdel where parent=".$data["parent"]." and type='".$data["type"]."'");
        if(mysql_num_rows($res)==1&&is_numeric(mysql_result($res,0,0)))
            $max_pos = mysql_result($res,0,0)+1;
        mysql_query("insert into razdel(name,parent,type,content_type,position) values('".trim($data["name"])."','".$data["parent"]."','".trim($data["type"])."','".trim($data["content_type"])."','".$max_pos."')");
        echo mysql_error();
    }
}

/**
 * ������� �������� �������. � �������� ��������� ���������� id-�������
 *
 * @param int $id
 */
function delete_razdel($id){
    if(is_numeric($id)){
        $result = mysql_query("select content_type from razdel where id=".$id);
        if(mysql_num_rows($result)>0){
            mysql_query("delete from ".mysql_result($result,0,0)." where razdel=".$id);
            mysql_query("delete from razdel where id='".$id."';");
        }
    }
}

/**
 * ������� �������������� �������. ������ ����� �� GET-�������
 *
 */
function rename_razdel(){
    $data = $_GET;
    if(trim($data["name"])!=""&&is_numeric($data["id"]))
        mysql_query("update razdel set name='".trim($data["name"])."' where id=".$data["id"]);
}

/**
 * ������� ���������� ������ ��� �������. ���������� ����� �������� �� ������ � ��������� ������ � ��, ������������� ������� �������
 *
 * @param int $razdel_id
 */
function set_icon($razdel_id){
    if(file_exists($_FILES["icon"]["tmp_name"])){
        ereg("\.([^\.]*)$",$_FILES["icon"]["name"],$t);
        $ext = $t[1];
        $fname = "razdel".$razdel_id.".".$ext;
        del_icon($razdel_id);
        copy($_FILES["icon"]["tmp_name"],ROOT_DIR.SUB_DIR.IMAGES_ICONS_DIR.$fname);
        mysql_query("update razdel set icon='".$fname."' where id='".$razdel_id."'");
    }
}

function del_icon($razdel_id){
    $res = mysql_query("select icon from razdel where id='".$razdel_id."'");
    if(mysql_num_rows($res)==1){
        $fname = mysql_result($res,0,0);
        if($fname!=""){
            mysql_query("update razdel set icon='' where id='".$razdel_id."'");
            unlink(ROOT_DIR.SUB_DIR."img/icons/".$fname);
        }
    }
}

function set_alias($razdel_id,$alias_id){
    if($alias_id!="b"&&is_numeric($alias_id)&&$razdel_id!=$alias_id){
        if(mysql_num_rows(mysql_query("select id from razdel where id=".$razdel_id))==1);
        mysql_query("update razdel set alias='".$alias_id."' where id='".$razdel_id."'");
    }
}

function change_parent($razdel_id,$parent){
    mysql_query("update razdel set parent=".$parent." where id=".$razdel_id);
}

function change_position($id,$order){
    $res = mysql_query("select position,parent,type from razdel where id='".$id."'");
    if(mysql_num_rows($res)==1&&($order=="up"||$order=="down")){
        $data = mysql_fetch_assoc($res);
        switch($order){
            case "up": $znak="<"; $term = "max"; break;
            case "down": $znak=">"; $term = "min"; break;
        }
        $res = mysql_query("select ".$term."(position) from razdel where position".$znak.$data["position"]." and parent='".$data["parent"]."' and type='".$data["type"]."'");
        if(mysql_num_rows($res)==1&&is_numeric(mysql_result($res,0,0))){
            mysql_query("update razdel set position=".$data["position"]." where position=".mysql_result($res,0,0)." and parent='".$data["parent"]."' and type='".$data["type"]."'");
            mysql_query("update razdel set position=".mysql_result($res,0,0)." where id='".$id."'");
        }
    }
}

function set_title($title,$id){
    mysql_query("update razdel set title='".$title."' where id='".$id."'");
}

function set_keywords($keywords,$id){
    mysql_query("update razdel set keywords='".$keywords."' where id='".$id."'");
}

function set_description($desc,$id){
    mysql_query("update razdel set description='".$desc."' where id='".$id."'");
}

function change_content_type($ct,$id){
    mysql_query("update razdel set content_type='".$ct."'where id='".$id."'");
}

function change_navi_type($nt,$id){
    mysql_query("update razdel set type='".$nt."' where id='".$id."'");
}

function set_main($id){
    mysql_query("update razdel set main='0' where main='1';");
    mysql_query("update razdel set main='1' where id='".$id."'");
}

function unset_main($id){
    mysql_query("update razdel set main='0' where id='".$id."'");
}

function enough(){
    $n = mysql_num_rows(mysql_query("select id from razdel where 1"));
    if($n<MAX_RAZDELS)
        return 0;
    else
        return 1;
}

function get_count(){
    $mas["used"] = mysql_num_rows(mysql_query("select id from razdel where 1"));
    $mas["available"] = MAX_RAZDELS - $mas["used"];
    return $mas;
}

function set_visible($razdel){
    if(isset($_GET["visible"]))
        mysql_query("update razdel set visible='1' where id='".$razdel."'");
    else
        mysql_query("update razdel set visible='0' where id='".$razdel."'");
}

function get_recursive_struct($par,$type,$incl){
    $arr = get_menu($par,$type);
    $target=array();
    for($i=0; $i<count($arr); $i++){
        $tmp=array("level" => $GLOBALS["level"],"incl" => $incl);
        if(have_children($arr[$i]["id"])){
            $childs=array();
            $GLOBALS["level"]++;
            $childs=get_recursive_struct($arr[$i]["id"],$type,$incl);
            $GLOBALS["level"]--;
            $tmp["childs"]=$childs;
        }
        $target[] = array_merge($arr[$i],$tmp);
    }
    return $target;
}

/**
 * ������� ��������� ����. �������, ��� ������������ ������ MVC, �� ��� ��� ������ ���������� ������� ������ ��������.
 * � ������ � ������ ���� �� ���� �� ������ �������
 *
 * @param array $v
 * @return string
 */
function draw_menu($v){
    $result="";
    for($i=0;$i<count($v);$i++){
        $result.='<table id="page_'.$v[$i]["id"].'" width="100%"><tr><td width="13px" style="background-image:url(\'img/';
        if($i==0){
            $result.='menu_tree_1.jpg';
        }elseif($i==count($v)-1){
            $result.='menu_tree_2.jpg';
        }else{
            $result.='menu_tree_3.jpg';
        }
        $result.='\');';
        if($i!=0&&$i!=count($v)-1){
            $result.='background-repeat:repeat-y;';
        }else{
            $result.='background-repeat:no-repeat;';
        }
        if($i==0){
            $result.='background-position:left bottom;';
        } if($i==count($v)-1){
            $result.='background-position:left top;';
        }
        $result.='"><img src="img/menu_tree_4.jpg"></td><td><table style="padding:1px; border:1px solid #ffffff;" width="100%" onMouseOver="this.style.border=\'1px dotted #acacac\';" onMouseOut="this.style.border=\'1px solid #ffffff\';"><tr><td style="padding-left:4px;"><a href="admin.php?work=razdel&from_razdel='.$v[$i]["id"].'"';
        if($v[$i]["main"]==1){
            $result.='style="font-weight:bold;"';
        }
        $result.='>'.$v[$i]["name"].'</a>';
        if($v[$i]["main"]==1){
            $result.='<span style="font-size:10px;"> (���������)</span>';
        }
        $result.='</td>';
        if(($v[$i]["incl"]==0||$v[$i]["level"]+1<$v[$i]["incl"])&&(MAX_RAZDELS==0||!enough())){
            $result.='<td width="10px" align="right"><a href="javascript:addSubPage_show(\'page_'.$v[$i]["id"].'\','.$v[$i]["id"].',\''.$v[$i]["type"].'\')"><img src="img/add_plus_icon_2.jpg" border="0"></a></td>';
        }
        $result.='</tr></table></td></tr>';
        if(isset($v[$i]["childs"])){
            $result.='<tr><td width="13px" style="';
            if($i!=count($v)-1){
                $result.='background-image:url(\'img/menu_tree_3.jpg\'); background-repeat:repeat-y;';
            }
            $result.='">&nbsp;</td> <td style="padding-left:10px;">'.draw_menu($v[$i]["childs"]).'</td></tr>';
        }
        $result.='
			</table>
		';
    }
    return $result;
}
