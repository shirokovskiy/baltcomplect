<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
	<script type="text/javascript" src="js/Message.js"></script>
	<script type="text/javascript" src="js/page_add_submit.js"></script>
	<script type="text/javascript" src="js/show_properties.js"></script>
	<script type="text/javascript" src="js/add_subpage.js"></script>
	<script type="text/javascript" src="js/js_lib.js"></script>

	<!--[if gte IE 5.5000]>
	<script language="JavaScript">
		function correctPNG() // correctly handle PNG transparency in Win IE 5.5 or higher.
		{
			for(var i=0; i<document.images.length; i++)
			{
				var img = document.images[i]
				var imgName = img.src.toUpperCase()
				if (imgName.substring(imgName.length-3, imgName.length) == "PNG")
				{
					var imgID = (img.id) ? "id='" + img.id + "' " : ""
					var imgClass = (img.className) ? "class='" + img.className + "' " : ""
					var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
					var imgStyle = "display:inline-block;" + img.style.cssText
					if (img.align == "left") imgStyle = "float:left;" + imgStyle
					if (img.align == "right") imgStyle = "float:right;" + imgStyle
					if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle
					var strNewHTML = "<span " + imgID + imgClass + imgTitle
						+ " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
						+ "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
						+ "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>"
					img.outerHTML = strNewHTML
					i = i-1
				}
			}
		}
		window.attachEvent("onload", correctPNG);
	</script>
	<![endif]-->
</head>
<body>

<div class="page_add_box" id="addSubPage" style="visibility:hidden; position:absolute;">
	<div class="page_add_title">���������� ����������</div>
	<form action='admin.php' id="page_add_form_2" class="no_spaces">
		<?php if(isset($_GET["from_razdel"])){?><input type="hidden" name="from_razdel" value="<?php echo $show_razdel["id"]?>"><?php }?>
		<input type="hidden" name="act" value="add_razdel">
		<input type='hidden' name='parent' value='0' id="par_id">
		<input type="hidden" name="type" id="par_type" value="">
		<div class="page_add_text">��������</div>
		<div><input type="text" class="page_add_els" name="name"></div>
		<div class="page_add_text">��� ������</div>
		<div>
			<?php if(count($content_types)>1){?>
				<select class="page_add_els" weight="100%" name="content_type">
					<option value="">-- �������� --</option>
					<?php for($i=0; $i<count($content_types); $i++){?>
						<option value="<?php echo $content_types[$i][1]?>"><?php echo $content_types[$i][0]?></option>
					<?php }?>
				</select>
			<?php }else{?><input type="hidden" name="content_type" value="<?php echo $content_types[0][1]?>"><?php }?>
		</div>
		<div class="page_add_text">
			<table>
				<tr>
					<td><a href="javascript:page_form_submit('page_add_form_2')" class="action_link"><img src="img/add_sub_icon.jpg" border="0"></a></td>
					<td style="padding-left:5px;"><a href="javascript:page_form_submit('page_add_form_2')" class="action_link">��������</a></td>
					<td style="padding-left:45px;"><a href="javascript:addSubpageCancel();" class="action_link"><img src="img/mess_cancel.jpg" border="0"></a></td>
					<td style="padding-left:5px;"><a href="javascript:addSubpageCancel();" class="action_link">������</a></td>
				</tr>
			</table>
		</div>
	</form>
</div>

<table class="full_table">
	<tr>
		<td id="head">
			<table class="full_table">
				<tr>
					<td><img src="img/head_title.jpg"></td>
					<td align="right">
						<table>
							<tr>
								<td style="padding-right:20px; color:#ffffff;">
									������������, <span style="font-weight:bold;"><?php echo $user_data["name"]?></span>
								</td>
								<td>
									<a href="admin.php?logout" style="color:#ffffff;">�����</a>
								</td>
							</tr>
						</table>

					</td>
					<td align="right" width="150px"><img src="img/head_logo.jpg"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td id="content">
			<table class="full_table">
				<tr>
					<td id="menu_block">
						<div id="menu_title">������� �����</div>
						<?php if(MAX_RAZDELS!=0){?>
							<div style="margin-bottom:15px;">
								<div>��������� ��������:     <?php echo $count_data["used"]?></div>
								<div>�������� ��� ����������: <?php echo $count_data["available"]?></div>
							</div>
						<?php }?>
						<div class="page_add_box">
							<div class="page_add_title">���������� ��������</div>
							<form action='admin.php' id="page_add_form" class="no_spaces">
								<?php if(isset($_GET["from_razdel"])){?><input type="hidden" name="from_razdel" value="<?php echo $show_razdel["id"]?>"><?php }?>
								<input type="hidden" name="act" value="add_razdel">
								<input type='hidden' name='parent' value='0'>
								<div class="page_add_text">��������</div>
								<div><input type="text" class="page_add_els" name="name"></div>
								<?php if(count($types)>1){?>
									<div class="page_add_text">��� ���������</div>
									<div>
										<select class="page_add_els" weight="100%" name="type">
											<option value="">-- �������� --</option>
											<?php for($i=0; $i<count($types); $i++){?>
												<option value="<?php echo $types[$i][1]?>"><?php echo $types[$i][0]?></option>
											<?php }?>
										</select>
									</div>
								<?php }else{?><input type="hidden" name="type" value="<?php echo $types[0][1]?>"><?php }?>
								<div class="page_add_text">��� ������</div>
								<div>
									<?php if(count($content_types)>1){?>
										<select class="page_add_els" weight="100%" name="content_type">
											<option value="">-- �������� --</option>
											<?php for($i=0; $i<count($content_types); $i++){?>
												<option value="<?php echo $content_types[$i][1]?>"><?php echo $content_types[$i][0]?></option>
											<?php }?>
										</select>
									<?php }else{?><input type="hidden" name="content_type" value="<?php echo $content_types[0][1]?>"><?php }?>
								</div>
								<div class="page_add_text">
									<table>
										<tr>
											<td><img src="img/add_plus_icon.jpg"></td>
											<td style="padding-left:5px;"><a <?php if(MAX_RAZDELS!=0&&enough()){?>onClick="enoughError();"<?php }?> href="<?php if(MAX_RAZDELS!=0&&enough()){?>###<?php }else{?>javascript:page_form_submit('page_add_form');<?php }?>" class="action_link">��������</a></td>
										</tr>
									</table>
								</div>
							</form>
						</div>

						<?php if($s_v==0){?>
							<?php $c=0; foreach($struct as $k=>$v){?>
								<div class="nav_margin">
									<table>
										<tr>
											<td><img src="img/icons/<?php echo $types[$c][2]?>"></td>
											<td class="nav_name"><?php echo $k?> ���������</td>
										</tr>
									</table>
								</div>
								<div class="nav_margin">
									<table id="nav_tab" width="100%">
										<tr>
											<td>
												<?php echo draw_menu($v)?>
											</td>
										</tr>
									</table>
								</div>
								<?php $c++;}?>
						<?php }else{?>
							<?php foreach($struct as $k=>$v)if(count($struct[$k]["p0"])>0){?>
								<div><span style="font-weight:bold;"><?php echo $k?> ���������</span></div>
								<table cellpadding=2px cellspacing=0>
									<?php for($i=0;$i<count($struct[$k]["p0"]);$i++){?>
										<tr>
										<td width="9px"><?php if(isset($struct[$k]["p".$struct[$k]["p0"][$i]["id"]])){?><img src='img/sh_ch.gif' onClick="show_children(<?php echo $struct[$k]["p0"][$i]["id"]?>)" id='ic<?php echo $struct[$k]["p0"][$i]["id"]?>'><?php }?></td><td><a href='admin.php?work=razdel&from_razdel=<?php echo $struct[$k]["p0"][$i]["id"]?>'><?php echo $struct[$k]["p0"][$i]["name"]?></a></td>
										<?php if(isset($struct[$k]["p".$struct[$k]["p0"][$i]["id"]])){?></tr><tr><td></td><td id='p<?php echo $struct[$k]["p0"][$i]["id"]?>' class="ch"></td><?php }?>
										</tr>
									<?php }?>
								</table>
							<?php }}?>
						<?php if(USE_VIRTUAL){?>
							<div class="page_add_text"><a href="admin.php?work=hidden_pages" class="page_add_title">����������� ��������</a></div>
						<?php }if(USE_VOTINGS){?>
							<div class="page_add_text"><a href="admin.php?work=votings" class="page_add_title">�����������</a></div>
						<?php }if(USE_CONFIG){?>
							<div class="page_add_text"><a href="admin.php?work=config" class="page_add_title">���������</a></div>
						<?php }if(USE_UPLOAD_BASE){?>
							<div class="page_add_text">
								<span class="title">��������� ����</span>
								<br><br>
								<form action="admin.php?act=read_dump" method="post" enctype="multipart/form-data">
									<input type="file" name="sql">  <br><br>
									<input type="submit" value="OK" class="but">
								</form>
							</div>
						<?php }if(USE_UPLOAD_CAT){?>
							<div class="page_add_text">
								<span class="title">��������� ������� (*.csv)</span>
								<br><br>
								<form action="admin.php?act=upload_catalog" method="post" enctype="multipart/form-data">
									<input type="file" name="catalog">  <br><br>
									<input type="submit" value="OK" class="but">
								</form>
							</div>
						<?php }?>
						<?php if(USE_UPLOAD_CAT&&($user_data["su"]==1||$user_data["su"]==2)){?><div class="page_add_text"><a href="admin.php?work=adm_users" class="page_add_title">������������ �������</a></div><?php }?>
						<div class="page_add_text"><a href="admin.php?work=ch_passwd" class="page_add_title">�������� ������</a></div>
					</td>
					<td valign="top">
						<?php require $page?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td id="footer">
			2005-2007 &copy; ������ ��� ������ ������
		</td>
	</tr>
</table>
</body>
</html>
