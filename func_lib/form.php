<?php
function check_data($string,$datatype){
	if(strlen(trim($string))==0)
		return 2;
	else
		switch($datatype){
			case "mail":
				if(preg_match("/(?i)^[a-zA-Z0-9._-]*@(?:[a-zA-z0-9-]+\.)+[a-zA-Z]{1,10}$/",$string))
					return 0;
				else
					return 1;
			default: return 0;
		}
}

class Form{
	var $method="";
	var $data_desc=array();
	var $errors=array();
	var $warnings=array();
	var $data=array();
	var $user_data=array();

	function Form($form_method,$data_desc){
		$this->method=$form_method;
		$this->data_desc=$data_desc;
	}

	function get_value($field){
		if(isset($this->user_data[$field]))
			return $this->user_data[$field];
		return "";
	}

	function get_error($field){
		if(isset($this->errors[$field]))
			return $this->errors[$field];
		return "";
	}

	function get_warning($field){
		if(isset($this->warnings[$field]))
			return $this->warnings[$field];
		return "";
	}

	function have_errors(){
		if(count($this->errors)==0)
			return false;
		return true;
	}

	function have_warnings(){
		if(count($this->warnings)==0)
			return false;
		return true;
	}

	function get_errors(){
		return $this->errors;
	}

	function get_warnings(){
		return $this->warnings;
	}

	function get_data(){
		return $this->data;
	}

	function getInsertQuery(){
		$already=false;
		$fields = "";
		$values = "";
		for($i=0;$i<count($this->data);$i++){
			if($already){
				$fields.=",";
				$values.=",";
			}
			$fields.=$this->data[$i][0];
			if(get_magic_quotes_gpc())
				$this->data[$i][1] = stripslashes($this->data[$i][1]);
			$values.="'".mysql_escape_string($this->data[$i][1])."'";
			$already=true;
		}
		return array($fields,$values);
	}

	function read(){
		$form_data=array();
		switch(strtolower($this->method)){
			case "get": $form_data=$_GET; break;
			case "post":
			default: $form_data=$_POST; break;
		}
		foreach($this->data_desc as $fld_name=>$fld_pars){
			if(isset($form_data[$fld_name])){
				$chd = check_data($form_data[$fld_name],$fld_pars[1]);
				if($chd==0){
					$this->data[]=array($fld_name,$form_data[$fld_name]);
					$this->user_data[$fld_name]=$form_data[$fld_name];
				}
				elseif($fld_pars[0]==1)
					$this->errors[$fld_name]=$chd;
				elseif($chd!=2)
					$this->warnings[$fld_name]=$chd;
			}
			elseif($fld_pars[0]==1)
				$this->errors[$fld_name]=3;
		}
	}
}
