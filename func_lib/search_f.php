<?php
function search($string){
	//vars
	$stype=2;
	$arr=array();
	$case="";
	$str_arr=array();
	$offset=200;

	//Getting type of search
	if(isset($_GET["stype"])&&$_GET["stype"]>-1)
		$stype=$_GET["stype"];

	//preparing string
	$cmp_str=$string;
	if(get_magic_quotes_gpc())
		$cmp_str=stripslashes($cmp_str);
	$cmp_str=preg_replace("/[()\[\].,;*+\"'\/\\\{}]/"," ",$cmp_str);
	$cmp_str=htmlspecialchars($cmp_str);
	$cmp_str=preg_replace("/\s{2,}/"," ",trim($cmp_str));
	$cmp_str=mysql_escape_string($cmp_str);

	//main block
	if($cmp_str!=""){
		$what = "razdel.name,razdel.title,razdel.id,data.content";
		$from = "razdel,data";
		$where = "razdel.id=data.razdel and (";
		if($stype==0){
			$str_arr[]=htmlspecialchars(trim($cmp_str));
		}
		else{
			$str_arr=explode(" ",$cmp_str);
			if($stype==1)
				$case="and";
			else
				$case="or";
		}
		$already=false;
		foreach($str_arr as $v){
			if($already)
				$where.=" ".$case." ";
			$already=true;
			$where.="data.content regexp '>[^<]*".$v."|^[^<]*".$v."'";
		}
		$where.=")";
		$order = "id";
		$res=mysql_query("select ".$what." from ".$from." where ".$where." order by ".$order);
		while($tmp=mysql_fetch_assoc($res)){
			$tmp["content"]=strip_tags($tmp["content"]);
			//getting announce
			preg_match("/(?i)((\b[^.,:;][^.]{0,$offset}|^)(?:".implode("|",$str_arr).")[^.]{0,$offset}\b\.?)/",$tmp["content"],$tc);
			$tmp["content"]=$tc[1];
			//mark found words
			$tmp["content"]=preg_replace("/(?i)(".implode("|",$str_arr).")/","<b>\\1</b>",$tmp["content"]);
			$arr[]=$tmp;
		}
	}
	return $arr;
}
