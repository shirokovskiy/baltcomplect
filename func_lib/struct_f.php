<?php
function get_menu($parent,$type=""){
	$arr = array();
	$where="1";
	if(strlen($type)>0)
		$where="type='".$type."'";
	$result = mysql_query("select * from razdel where visible='1' and parent='".$parent."' and ".$where." order by type,position");
	$i=0;
	while($tmp = mysql_fetch_assoc($result)){
		$arr[$i] = $tmp;
		$arr[$i]["link"]="";
		switch($tmp["content_type"]){
			case "download":
				$r=mysql_query("select name from files where razdel='".$tmp["id"]."'");
				if(mysql_num_rows($r)==1)
					$arr[$i]["link"].="/downloads/".mysql_result($r,0,0);
				else
					$arr[$i]["link"]="";
				break;
			case "bbforum":
				$arr[$i]["link"].="/forum/";
				break;
			case "main":
				$arr[$i]["link"].="/";
				break;
			default:
				$arr[$i]["link"].="/pages/".$tmp["id"]."/";
				break;
		}
		$i++;
	}
	return $arr;
}

function get_tree($parent,$type){
	$arr=array();
	$tmp=get_menu($parent,$type);
	for($i=0; $i<count($tmp); $i++){
		$arr[$i]["par"]=$tmp[$i];
		$arr[$i]["child"]=get_menu($tmp[$i]["id"],"0");
	}
	return $arr;
}

function get_razdel_data($id){
	$arr = array();
	$result = mysql_query("select * from razdel where id='".$id."'");
	if(mysql_num_rows($result))
		$arr = mysql_fetch_array($result);
	return $arr;
}

function get_alias($id){
	$result = mysql_query("select alias from razdel where id='".$id."'");
	if(mysql_num_rows($result)==1)
		return mysql_result($result,0,0);
	return -1;
}

function get_parent($id){
	$result = mysql_query("select parent from razdel where id='".$id."'");
	if(mysql_num_rows($result)==1)
		return mysql_result($result,0,0);
}

function get_main(){
	$result = mysql_query("select id from razdel where main='1'");
	if(mysql_num_rows($result)==1)
		return mysql_result($result,0,0);
	return 0;
}

function get_real_id($id){
	$alias = get_alias($id);
	if($alias == -1)
		$GLOBALS["current_id"] = 0;
	elseif($alias==0)
		$GLOBALS["current_id"] = $id;
	else
		get_real_id($alias);
}

function have_children($parent){
	if(is_numeric($parent))
		if(mysql_num_rows(mysql_query("select id from razdel where parent='".$parent."'")))
			return true;
	return false;
}

function get_name_by_id($id){
	$result = mysql_query("select name from razdel where id='".$id."'");
	echo mysql_error();
	if(mysql_num_rows($result))
		return mysql_result($result,0,0);
	return "";
}

function get_path($id){
	$parent = get_parent($id);
	if($parent!=0)
		get_path($parent);
	$GLOBALS["par_list"][]=array(get_name_by_id($id),$id);
}

function getFullTree($parent,$navi,$level=0,$level_limit=-1){
	$result=array();
	$tmp=get_menu($parent,$navi);
	for($i=0;$i<count($tmp);$i++){
		$result[]=array_merge($tmp[$i],array("level"=>$level));
		if(have_children($tmp[$i]["id"])&&($level_limit==-1||$level<$level_limit))
			$result=array_merge($result,getFullTree($tmp[$i]["id"],$navi,$level+1,$level_limit));
	}
	return $result;
}

function show_tree($parent){
	echo "<table cellspacing=0 cellpadding=2>";
	$menu=get_menu($parent,"hor");
	foreach($menu as $k=>$v){
		echo "<tr><td class='svyaz'";
		if($k==count($menu)-1)
			echo " id='end'";
		echo ">-</td><td>".$v["name"]."</td></tr>";
		if(have_children($v["id"])){
			echo "<tr><td class='svyaz'></td><td>";
			show_tree($v["id"]);
			echo "</td></tr>";
		}
	}
	echo "</table>";
}
