<?php
Define("IN_SITE",1);
//functions
require "sql.php";
require "func_lib/struct_f.php";
require "func_lib/data_f.php";
require "func_lib/common.php";
require "func_lib/search_f.php";
require "func_lib/news_f.php";
require "func_lib/form_f.php";
require "func_lib/form.php";
require "func_lib/votings_f.php";
require "func_lib/transporter.php";
require "func_lib/questions.php";

//initialising variables
$navi = get_menu(0,"hor");
$vert = get_menu(0,"vert");
for($i=0;$i<count($vert);$i++){
	$vert[$i]["childs"]=get_menu($vert[$i]["id"],"vert");
}
$par_list = array();
$path_ar = array();
$path_string = "";
$children = array();
$title_head = "";
$title_content = "";
$keywords = "";
$description = "";
$show_content = "";
$current_parent = 0;
$root=0;
$current_id = 0;
$separator = " &raquo; ";
$level = 0;
$main_page = false;
$news=get_news(3);

//configurations
$config=array();
$res = mysql_query("select * from config where 1");
while($tmp=mysql_fetch_assoc($res))
	$config[$tmp["config_name"]]=$tmp["config_value"];

for($i=0;$i<count($navi);$i++){
	if(have_children($navi[$i]["id"])){
		$hor_ch["par".$navi[$i]["id"]]=get_menu($navi[$i]["id"],"");
	}
}

//analysing user request, getting current page content
if(isset($_GET["type"]))
	$t = $_GET["type"];
else
	$t = "pages";

switch($t){
	case "pages":
		if(isset($_GET["id"])&&is_numeric($_GET["id"])) {
			get_real_id($_GET["id"]);
		} else {
			get_real_id(get_main());
		}
		if(($razdel_data = get_by_id($current_id,"razdel"))!==false){
			$need_content = true;
			get_path($current_id);
			$root=$par_list[0][1];
			$level=count($par_list);

			//getting children
			if(have_children($current_id))
				$current_pare00nt=$current_id;
			elseif($level>1)
				$current_parent=$razdel_data["parent"];
			if($current_parent>0)
				$children=get_menu($current_parent,"");

			//getting page info
			$keywords = $razdel_data["keywords"];
			$description = $razdel_data["description"];
			if($razdel_data["main"]==1)
				$main_page=true;
			else
				$path_ar[]=array("name"=>"�������","link"=>"/");
			$title_content = $razdel_data["name"];
			$title_head=($razdel_data["title"]!="")?$razdel_data["title"]:$title_content;

			//getting path
			for($i=0;$i<count($par_list)-1;$i++)
				$path_ar[]=array("name"=>$par_list[$i][0],"link"=>"/pages/".$par_list[$i][1]."/");

			if($need_content)
				switch($razdel_data["content_type"]){
					case "data":
						$content = get_text_data($current_id);
						$att_files = get_from_base("*","attache_files","razdel=".$current_id,"id");
						ob_start();
						require "simple_data.php";
						$show_content=ob_get_contents();
						ob_end_clean();
						break;
					case "catalog":
						if(isset($_GET["uid"])&&is_numeric($_GET["uid"])){
							$data=get_by_id($_GET["uid"],"catalog");
							ob_start();
							require "details.php";
							$show_content=ob_get_contents();
							ob_end_clean();
						}else{
							$text = get_text_data($current_id);
							$data = get_catalog_data($current_id);
							ob_start();
							require "catalog.php";
							$show_content=ob_get_contents();
							ob_end_clean();
						}
						break;
					case "gallery":
						$data = get_from_base("*","gallery","razdel=".$current_id,"id desc");
						ob_start();
						require "gallery.php";
						$show_content=ob_get_contents();
						ob_end_clean();
						break;
					case "photos":
						$text = get_text_data($current_id);
						$data = get_from_base("*","photos","razdel=".$current_id,"pos ");
						ob_start();
						require "photos.php";
						$show_content=ob_get_contents();
						ob_end_clean();
						break;
					case "form":
						if(isset($_GET["sent"]))
							$show_content = "���� ������ ����������";
						else{
							$sent = 0;
							$error = array();
							$user_data=array("name"=>"","mail"=>"","content"=>"");
							if(isset($_GET["send"]))
								$sent = send_form($user_data,$error);
							if($sent==0){
								$text = get_text_data($current_id);
								ob_start();
								require "form.php";
								$show_content=ob_get_contents();
								ob_end_clean();
							}
							elseif($sent==1)
								Header("Location: /pages/".$current_id."/?sent");
							unset($user_data);
							unset($error);
						}
						break;
					case "gb":
						$dt = array("name"=>array(1,"text"),
							"mail"=>array(1,"mail"),
							"title"=>array(1,"text"),
							"content"=>array(1,"text"));
						$form = &new form("post",$dt);
						if(isset($_GET["act"])){
							$form->read();
							if(!$form->have_errors()){
								$query=$form->getInsertQuery();
								mysql_query("insert into gb(date,razdel,".$query[0].") values('".time()."','".$current_id."',".$query[1].")");
								Header("Location: /pages/".$current_id."/");
							}
						}
						$data = get_from_base("*","gb","public=1 and razdel=".$current_id,"id desc");
						ob_start();
						require "gb.php";
						$show_content=ob_get_contents();
						ob_end_clean();
						break;
					case "news":
						if(isset($_GET["uid"])&&is_numeric($_GET["uid"])&&($data=get_by_id($_GET["uid"],"news"))!==false){
							$path_ar[]=array("name"=>$razdel_data["name"],"link"=>"/pages/".$current_id."/");
							$title_content=$title_head=$data["title"];
							$show_content=$data["content"];
						}else{
							$data = get_from_base("date,announce,id,razdel,title","news","razdel=".$current_id,"id desc");
							ob_start();
							require "news.php";
							$show_content=ob_get_contents();
							ob_end_clean();
						}
						break;
					case "questions":
						if(isset($_GET["act"]))
							if(($_GET["act"] == "add_question")&&isset($_POST["question"])){
								if(add_question($_POST["question"]))
									Header("Location: /pages/".$current_id."/?sent=1");
								else
									Header("Location: /pages/".$current_id."/?sent=0");
							}
						$qu = get_questions();
						ob_start();
						require "questions.php";
						$show_content=ob_get_contents();
						ob_end_clean();
						break;
				}
			unset($need_content);
		}
		break;

	case "search":
		if(isset($_GET["string"])&&trim($_GET["string"])!=""){
			$data = search($_GET["string"]);
			$title_content="&laquo;".htmlspecialchars($_GET["string"])."&raquo;";
			$title_head="�����";
			ob_start();
			require "show_search.php";
			$show_content=ob_get_contents();
			ob_end_clean();
		}
		else
			$title_content = $title_head = "�����. ������ ������ ������";
		break;

	case "cart":
		$title_content=$title_head="������� �������";
		if(!isset($_GET["sent"])){
			$dt = array("name"=>array(1,"text"),
				"mail"=>array(1,"mail"),
				"phone"=>array(0,"text"),
				"address"=>array(0,"text"),
				"content"=>array(0,"text"));
			$form = &new form("post",$dt);
			if(isset($_GET["action"]))
				switch($_GET["action"]){
					case "add":
						if(isset($_GET["g"])&&is_numeric($_GET["g"]))
							add_to_basket($_GET["g"],1);
						break;
					case "update": update_basket(); break;
					case "send":
						$form->read();
						if(!$form->have_errors()){
							$query=$form->getInsertQuery();
							$zakaz=str_replace("\n","<br>",show_basket("str"));
							mysql_query("insert into zakaz(date,".$query[0].",zakaz) values(".time().",".$query[1].",'".$zakaz."')");
							Header("Location: /cart/?sent");
						}
						break;
				}
			$tmp = show_basket();
			if(count($tmp)>0){
				ob_start();
				require "basket.php";
				$show_content=ob_get_contents();
				ob_end_clean();
			}else{
				$show_content="������� �����";
			}
		}else{
			$show_content="��� ����� ������";
		}
		break;

	case "sitemap":
		$title_head=$title_content="����� �����";
		$data=array_merge(getFullTree(0,"hor",0,-1),getFullTree(0,"vert",0,-1));
		ob_start();
		require "map.php";
		$show_content=ob_get_contents();
		ob_end_clean();
		break;

	case "voting":
		if(isset($_GET["theme"])){
			if(isset($_GET["act"]))
				if($_GET["act"]=="add_vote"&&isset($_GET["vote"])&&is_numeric($_GET["vote"]))
					if(add_vote($_GET["vote"]))
						$show_content = "<span style='font-weight:bold;'>��� ����� ��������</span><br><br>";
					else
						$show_content = "<span style='font-weight:bold;'>�� ��� ���������� �� ����� �������</span><br><br>";

			$q = get_vote_question($_GET["theme"]);
			$title_content=$title_head=$q["name"];
			$par_list[] = array($q["name"].". ����������",0);

			$vote_answers = get_voting_stat($_GET["theme"]);
			ob_start();
			require "voting.php";
			$show_content.=ob_get_contents();
			ob_end_clean();
		}
		break;

}

for($i=0;$i<count($path_ar);$i++){
	if($path_ar[$i]["link"]!=""){
		$path_string.=$separator."<a href=".$path_ar[$i]["link"]." class=path>".$path_ar[$i]["name"]."</a>";
	}}

header('Content-Type: text/html; charset=windows-1251');

require "view.php";
