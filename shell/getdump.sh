#!/bin/bash
date
#
#
#
# @author   Dmitriy Shirokovskiy
#
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd ..
ROOT_DIR="$(pwd)"
CUR_DATE=$(date +%Y-%m-%d__%H.%M)
BKP_DIR=$ROOT_DIR/var/backups
DBNAME=b107087_bc
DBFILE=$DBNAME.$CUR_DATE.sql

DESTINATION=$BKP_DIR/$DBFILE
#DESTINATIONBZ=$DESTINATION.bz2

echo "Start backup remote database: $DBNAME"
ssh baltcomplect "mysqldump --defaults-extra-file=mydbsql.cnf --opt $DBNAME " > $DESTINATION

if [ -f $DESTINATION ];
then
    echo "Run DB restore process"
    mysql --defaults-extra-file=$THIS_DIR/mydbsql.cnf $DBNAME < $DESTINATION

    echo "Start zip: $DESTINATION"
    bzip2 -9 $DESTINATION

    #echo "Download files"
    #rsync -huvar --exclude=wstat baltcomplect:baltcomplectru/www/* ROOT_DIR/
else
    echo "File has not downloaded: $DESTINATION"
fi

echo "The End"
date
